# ci-buildtools

Creates cross-compilation tools for building libffcall.

# The build environments

This project contains jobs that create build environments for execution of
ci-distcheck.

Each build environment is in fact a build image (as a docker image) that
contains
  * the prerequisite packages (like make, gcc),
  * one or more cross-compilers for each supported CPU type.

1. It is described through a Dockerfile in a subdirectory of this project,
   and a 'generate-*' job in the .gitlab-ci.yml of this project.

2. Executing the job through the Gitlab UI:
   [CI/CD > Jobs](https://gitlab.com/gnu-libffcall/ci-buildtools/-/jobs)
   creates the corresponding docker image.

3. Each build image is then visible in the Gitlab UI:
   [Packages > Container Registry](https://gitlab.com/gnu-libffcall/ci-buildtools/container_registry).
